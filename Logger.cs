﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace DLL_Logger
{
    public class Logger : IDisposable
    {
        private static object lockObject = new object();

        /// <summary>
        /// Path to logfile
        /// 
        /// <para>
        ///     Default value: errors.log
        /// </para>
        /// </summary>
        public string FilePath { get; set; } = "errors.log";

        /// <summary>
        /// Delimiter of the fields
        /// 
        /// <para>
        ///     Default value: ;
        /// </para>
        /// </summary>
        public string Delimiter { get; set; } = ";";

        /// <summary>
        /// String which is used to format the current datetime
        /// </summary>
        public string DateTimeFormat { get; set; } = string.Empty;


        /// <summary>
        /// Defines whether the stream should close after each writing
        /// 
        /// <para>
        ///     Default value: true
        /// </para>
        /// </summary>
        public bool CloseStreamAfterEachWriting { get; set; } = true;

        /// <summary>
        /// Defines whether the stream should instantly write the message
        /// into the file
        /// 
        /// <para>
        ///     Default value: false
        /// </para>
        /// </summary>
        public bool FlushInstantly { get; set; }

        /// <summary>
        /// Defines whether the current datetime should be added
        /// 
        /// <para>
        ///     Default value: true
        /// </para>
        /// </summary>
        public bool AddCurrentDateTime { get; set; } = true;

        /// <summary>
        /// Defines whether the current logined windows user should be added
        /// 
        /// <para>
        ///     Default value: false
        /// </para>
        /// </summary>
        public bool AddWindowsUser { get; set; }

        /// <summary>
        /// Defines whether the message should be also printed in the debugger
        /// 
        /// <para>
        ///     Default value: false
        /// </para>
        /// </summary>
        public bool PrintAlsoInDebugger { get; set; }

        /// <summary>
        /// StreamWriter which is used
        /// </summary>
        public StreamWriter StreamWriter { get; private set; }

        /// <summary>
        /// Logs a message
        /// </summary>
        /// <param name="message">Message to log</param>
        public void Log(string message)
        {
            lock (lockObject)
            {
                if (this.StreamWriter == null)
                    this.StreamWriter = new StreamWriter(this.FilePath, true);

                string messageToLog = DateTime.Now.ToString(this.DateTimeFormat)
                                          + this.Delimiter
                                          + (this.AddWindowsUser ? Environment.UserName + this.Delimiter : "")
                                          + message;

                this.StreamWriter.WriteLine(messageToLog);

                if (this.PrintAlsoInDebugger)
                    Debug.WriteLine(messageToLog);

                if (this.FlushInstantly)
                    this.StreamWriter.Flush();

                if (this.CloseStreamAfterEachWriting)
                    this.CloseWriter();
            }
        }

        /// <summary>
        /// Logs a message asynchronously
        /// </summary>
        /// <param name="message">Message to log</param>
        public async Task LogAsync(string message)
        {
            await Task.Run(() => this.Log(message));
        }

        /// <summary>
        /// Closes the stream
        /// </summary>
        public void CloseWriter()
        {
            if (this.StreamWriter == null)
                return;

            this.StreamWriter.Close();
            this.StreamWriter = null;
        }

        public void Dispose()
        {
            this.CloseWriter();
        }
    }
}
