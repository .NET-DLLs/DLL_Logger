# Verwendung

### Synchron
```cs
public void LogSomething()
{
    using (Logger logger = new Logger())
        logger.Log("Error!");
}
```

### Asynchron
```cs
public async void LogSomethingAsync()
{
    using (Logger logger = new Logger())
        await logger.LogAsync("Error!");
}
```